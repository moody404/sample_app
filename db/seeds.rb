User.create!(name:  "Ahmad Abotti",
             email: "h50@hotmail.com",
             password:              "22332233",
             password_confirmation: "22332233",
             admin: true ,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@hotmail.com"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end